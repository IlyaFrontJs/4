import {PeopleList} from './items-lists'
import PersonDetail from './person-detail';

export {PeopleList, PersonDetail}
